﻿using System.Text.Json;

string content = File.ReadAllText("item.json");
Todo item = JsonSerializer.Deserialize<Todo>(content);

Console.WriteLine(item.TaskTitle);

string content2 = File.ReadAllText("list-item.json");
List<Todo> todos = JsonSerializer.Deserialize<List<Todo>>(content2);

foreach (Todo todo in todos)
{
    Console.WriteLine(todo.TaskTitle);
}

public class Todo
{
    public DateTime CreatedAt { get; set; }
    public string TaskTitle { get; set; }
    public bool IsComplete { get; set; }
    public string Id { get; set; }
}

